EXTRN LEERNUM:FAR, IMPNUM:FAR, SALTO:FAR, ELEVARP:FAR, FAHACEL:FAR, CELAFAH:FAR, INVERTIR:FAR, SUMATORIA:FAR

STACKSG SEGMENT PARA STACK 'STACK'
    DB 512 DUP(0)               
STACKSG ENDS

DATA SEGMENT PARA PUBLIC 'DATA'
    MSMENU  DB '-----------------------MENU-----------------------',13,10
            DB '1.- Ingresar numero de 3 cifras y guardarlo en AX.',13,10
            DB '2.- Imprimir numero de 3 cifras guardado en AX.',13,10
            DB '3.- Elevar numero a una potencia.',13,10
            DB '4.- Convertir grados Fahrenheit a Celsius.',13,10
            DB '5.- Convertir grados Celsius a Fahrenheit.',13,10
            DB '6.- Invertir numero de tres cifras guardado en AX',10,13
            DB '7.- Llenar arreglo y calcular la suma',10,13
            DB '0.- Salir',10,13
            DB '---------Ingrese el numero de la operacion--------',10,13,'$'
    MSN DB 'HOLA',10,13,'$'
DATA ENDS

CODIGO SEGMENT PARA PUBLIC 'C0DE'
 MAIN PROC FAR
   ASSUME CS:CODIGO,DS:DATA,SS:STACKSG
   
        PUSH DS
        MOV AX,0
        PUSH AX
        MOV AX, SEG DATA
        MOV DS,AX
        
    ;IMPRIMIR MENSAJE MENU Y ELEGIR OPERACION
        MENU:
            MOV AH, 09
            MOV DX, OFFSET MSMENU
            INT 21H
            
            MOV AH, 01
            INT 21H
            SUB AL, 48
            
            CALL SALTO
            
            CMP AL, 0
            JZ FIN
            
            CMP AL, 1
            JE UNO
            
            CMP AL, 2
            JE DOS
            
            CMP AL, 3
            JE TRES
            
            CMP AL, 4
            JE CUATRO
            
            CMP AL, 5
            JE CINCO
            
            CMP AL, 6
            JE SEIS
            
            CMP AL, 7
            JE SIETE
            JMP MENU
    ;FIN 
    
        UNO:
            CALL LEERNUM
            MOV BX, AX
            
            CALL SALTO
            JMP MENU
           
        DOS:
            MOV AX, BX
            CALL IMPNUM
        
            CALL SALTO
            JMP MENU
            
        TRES:
            CALL ELEVARP
            CALL SALTO
            JMP MENU
        
        CUATRO:
            CALL FAHACEL
            CALL SALTO
            JMP MENU
        
        CINCO:
            CALL CELAFAH
            CALL SALTO
            JMP MENU
        
        SEIS:
            MOV AX, BX
            CALL INVERTIR
            CALL SALTO
            JMP MENU
        
        SIETE:
            CALL SUMATORIA
            CALL SALTO
            JMP MENU
        
        FIN:
            .EXIT
        ;El codigo anterior es de base para el proceso del programa
        ;Apartir de aqui escribimos nuestro programa
        ;mov ax, num  
        ;call impnum
        
        ;mov dl, 10
        ;mov ah, 02
        ;int 21h
        
        ;call leernum
        
        ;push ax
        
        ;mov dl, 10
        ;mov ah, 02
        ;int 21h
        
        ;pop ax
        ;call impnum
   
   RET
   MAIN ENDP 
  CODIGO ENDS
 END MAIN
