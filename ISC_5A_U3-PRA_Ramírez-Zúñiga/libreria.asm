STACKSG SEGMENT PARA STACK 'STACK'
   DB 512 DUP(0)              
STACKSG ENDS

DATA SEGMENT PARA PUBLIC 'DATA'
	MSNSALTO DB '',10,13,'$'
	MSNELEVAR DB 'INGRESA EL NUMERO Y LA POTENCIA: ',10,13,'$'
	MSNRESUL DB 'RESULTADO: ',10,13,'$'
	MSNGRADOS DB 'INGRESA LOS GRADOS: ',10,13,'$'
	MSNSUM DB 'INGRESE 10 NUMEROS DE DOS DIGITOS: ',10,13,'$'
	VECTOR DB 10 DUP(?),'$'
	SUMAS DW ?,10,13,'$'
	POTE DW ?,10,13,'$'
DATA ENDS

CODIGO SEGMENT PARA PUBLIC 'C0DE'
   ASSUME CS:CODIGO,DS:DATA,SS:STACKSG
   public LEERNUM, IMPNUM, SALTO, ELEVARP, FAHACEL, CELAFAH, INVERTIR, SUMATORIA
   
   ;PROCEDIMIENTO PARA LEER UN NÚMERO DE TRES CIFRAS
   ;COLOCANDOLO EN AX
	LEERNUM PROC FAR
	
        MOV AH, 01
        INT 21H 
        SUB AL, 48 
        MOV CL, 100 
        MUL CL 
        MOV DX, AX
        
        MOV AH, 01
        INT 21H
        SUB AL, 48
        MOV CL, 10
        MUL CL
        ADD DX, AX
        
        MOV AH, 01
        INT 21H
        SUB AL, 48
        CBW 
        ADD DX, AX
        MOV AX, DX
        
        RET
    LEERNUM ENDP
	;FIN PROCEDIMIENTO LEER NUMERO 3 CIFRAS
	
	;PROCEDIMIENTO PARA IMPRIMIR UN NUMERO DE 3 CIFRAS
	;PUESTO EN AX
	IMPNUM PROC FAR
    
        MOV DL,100
        DIV DL
        MOV CL, AH
        ;PUSH AX
        MOV DL, AL
        ADD DL,48
        MOV AH,02
        INT 21H
        
        ;POP AX
        MOV AL, CL
        CBW
        MOV DL, 10
        DIV DL
        MOV CL, AH 
        ;PUSH AX
        MOV DL, AL
        ADD DL, 48
        MOV AH, 02
        INT 21H
        
        ;POP AX
        MOV DL, CL
        ADD DL, 48
        MOV AH, 02
        INT 21H
        
        ret
    IMPNUM ENDP
    ;FIN PROCEDIMIENTO PARA IMPRIMIR UN NÚMERO DE 3 CIFRAS
    
    ;PROCEDIMIENTO PARA DAR UN SALTO DE LINEA
    SALTO PROC FAR
        MOV AH, 09
        MOV DX, OFFSET MSNSALTO
        INT 21H
        
        RET
    SALTO ENDP
    ;FIN PROCEDIMIENTO SALTO
    
    ;PROCEDIMIENTO ELEVAR NÚMERO DE UNA CIFRA A ALGUNA POTENCIA DE UNA CIFRA
    ELEVARP PROC FAR
        MOV AH, 09
        MOV DX, OFFSET MSNELEVAR
        INT 21H
        
        MOV AH, 01
        INT 21H
        SUB AL, 48
        MOV BL, AL
        
        MOV AH, 02
        MOV DL, 94
        INT 21H 
        
        MOV AH, 01
        INT 21H
        SUB AL, 48
        CBW
        
        MOV CX, AX
        SUB CX, 1
        MOV AL, BL
        CBW
        CICLO:
            PUSH CX
            
            MOV CL, BL
            MUL CL
            
            POP CX
        LOOP CICLO
        
        CALL SALTO
        
        MOV POTE, AX
        CMP AX, 10
        JAE DOSC
        JB UNAC
        
        DOSC:
            CMP AX, 99
            JA TRESC
            
            MOV CH, 10
            MOV AX, DX
            DIV CH
            
            MOV BL, AL
            MOV BH, AH
            
            MOV AH, 02
            MOV DL, BL
            ADD DL, 48
            INT 21H
            
            MOV AH, 02
            MOV DL, BH
            ADD DL, 48
            INT 21H
            JMP FINP
            
        TRESC:
            MOV AX, POTE
            CALL IMPNUM
            JMP FINP
    
        UNAC:
            POP AX
            MOV AH, 02
            MOV DX, AX
            ADD DX, 48
            INT 21H 
            
            JMP FINP
            
        FINP:
            RET
        
    ELEVARP ENDP
    ;FIN PROCEDIMIENTO ELEVARP
    
    ;PROCEDIMIENTO PARA convertir FAHRENHEIT A CELSIUS
    ;C=
    FAHACEL PROC FAR
        MOV AH, 09
        MOV DX, OFFSET MSNGRADOS
        INT 21H
        
        MOV AH, 01
        INT 21H
        MOV CH, 10
        MUL CH
        MOV BL, AL
        SUB BL, 48
        
        MOV AH, 01
        INT 21H
        SUB AL, 48
        ADD AL, BL
        
        MOV CL, 32
        SUB Al, CL
        MOV Cl, 5
        IMUL CL
        MOV CL, 9
        IDIV CL
    
        mov dh, ah
        mov ah, 02
        mov dl, al
        int 21h
        
        mov ah, 02
        mov dl, dh
        int 21h
        ;mov result1, al
        
        
            RET
            
    FAHACEL ENDP
    ;FIN PROCEDIMIENTO 
    
    ;PROCEDIMIENTO PARA 
    CELAFAH PROC FAR
        MOV AH, 09
        MOV DX, OFFSET MSNGRADOS
        INT 21H
        
        MOV AH, 01
        INT 21H
        MOV BL, AL
        SUB BL, 48
        
        MOV AH, 01
        INT 21H
        SUB AL, 48
        ADD BL, AL
        
        mov ah, 02
        mov dl, bl
        int 21h
        
        mov cl, bl
        mov al, 9
        imul cl
        mov cl, 5
        idiv cl
        add al, 32
       
        ;mov result2, al
        
        CMP al, 9
        JA DOSCF
        
        UNACF:
            MOV AH, 02
            MOV DL, AL
            ADD DL, 48
            INT 21H
            JMP FINCF
        
        DOSCF:
            MOV CH, 10
            DIV CH
            MOV DL, AL
            MOV CH, AH
            
            MOV AH, 02
            ADD DL, 48
            INT 21H
            
            MOV AH, 02
            ADD CH, 48
            INT 21H
            
        FINCF:
            RET
        
        RET
    CELAFAH ENDP
    ;FIN PROCEDIMIENTO 
    
    ;PROCEDIMIENTO PARA INVERTIR DIGITOS DE UN NÚMERO EN AX
    INVERTIR PROC FAR
        MOV CH, 10
        DIV CH
        MOV BL, AH
        MOV BH, AL
        
        MOV AH, 02
        MOV DL, BL
        ADD DL, 48
        INT 21H
        
        MOV AL, BH
        CBW
        MOV CH, 10
        DIV CH
        MOV BL, AH
        MOV BH, AL
        
        MOV AH, 02
        MOV DL, BL
        ADD DL, 48
        INT 21H
        
        MOV AH, 02
        MOV DL, BH
        ADD DL, 48
        INT 21H
    RET
    INVERTIR ENDP
    
    ;PROCEDIMIENTO PARA LLENAR ARREGLO Y CALCULE LA SUMAROTIA DE LOS MAYORES DE 10 Y MENORES QUE 50
    SUMATORIA PROC FAR
    
        MOV AH, 09
        MOV DX, OFFSET MSNSUM
        INT 21H
        
        MOV AH, 09
        MOV DX, OFFSET MSNSALTO
        INT 21H
        
        MOV CX, 10
        MOV SI, 0
        MOV DX, 0
        MOV SUMAS, 0
        CICLO2:
            MOV AH, 01
            INT 21H
            SUB AL, 48
            MOV BL, 10
            MUL BL
            MOV BX, AX
            
            MOV AH, 01
            INT 21H
            SUB AL, 48
            MOV AH, 0
            ADD BX, AX
            
            MOV AH, 02
            MOV DL, 32
            INT 21H
            
            MOV VECTOR[SI], BL
            INC SI
            
            MOV AL, BL
            CBW
            CMP AX, 10
            JA TALV
            JBE NEXT
            
            TALV:
                CMP AX, 50
                JB SUMA
                JAE NEXT
            
            SUMA:
                ADD SUMAS, AX
                JMP NEXT

            NEXT:
            INC DI
            
        LOOP CICLO2
        
        MOV AH, 02
        MOV DX, 10
        INT 21H
        
        MOV DX, SUMAS
        CMP DX, 10
        JBE DOS
        
        DOS:
            CMP DX, 99
            JA TRES
            
            MOV CH, 10
            MOV AX, DX
            DIV CH
            
            MOV BL, AL
            MOV BH, AH
            
            MOV AH, 02
            MOV DL, BL
            ADD DL, 48
            INT 21H
            
            MOV AH, 02
            MOV DL, BH
            ADD DL, 48
            INT 21H
            JMP FINS
        
        TRES:
            MOV CH, 100
            MOV AX, DX
            DIV CH
            
            MOV BL, AL
            MOV BH, AH
            
            MOV AH, 02
            MOV DL, BL
            ADD DL, 48
            INT 21H
            
            MOV CH, 10
            MOV AL, BH
            CBW
            DIV CH
            
            MOV BL, AL
            MOV BH, AH
            
            MOV AH, 02
            MOV DL, BL
            ADD DL, 48
            INT 21H
            
            MOV AH, 02
            MOV DL, BH
            ADD DL, 48
            INT 21H
        
        FINS:
            RET
    SUMATORIA ENDP
    
    
                
  CODIGO ENDS
 END 
 
