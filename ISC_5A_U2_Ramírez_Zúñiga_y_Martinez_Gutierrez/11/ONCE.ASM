;listooooooooooooooooooo
;
SEGPILA SEGMENT PARA STACK 'STACK'
        DB 512 DUP(0)
SEGPILA ENDS

SEGDATA SEGMENT PARA PUBLIC 'DATA'
		MSN DB "INGRESE UNA CADENA: ",10,13,'$'
		CADENA DB 100 DUP(" "),'$'
		MSNM DB "CADENA EN MAYUSCULAS: ",10,13,'$'
		CARA DB ?,'$'
SEGDATA ENDS

SEGCODE SEGMENT PARA PUBLIC 'CODE'
    MAIN PROC FAR
      ASSUME SS:SEGPILA,DS:SEGDATA,CS:SEGCODE

        PUSH DS
        MOV AX, 0
        PUSH AX
        MOV AX, seg SEGDATA
        MOV DS, AX
        
        ;IMPRIMIR MENSAJE
        MOV AH, 09
        MOV DX, OFFSET MSN
        INT 21H
        ;FIN
        
        ;INGRESAR CADENA
        MOV AH, 3FH
        MOV BX, 00
        MOV CX, 100
        MOV DX, OFFSET[CADENA]
        INT 21H
        ;FIN 
        
        ;IMPIRMER MENSAJE 2
        MOV AH, 09
        MOV DL, OFFSET MSNM
        INT 21H
        ;FIN
        
        MOV CX, OFFSET[CADENA]
        MOV SI, 0
        CICLO:
            MOV BL, CADENA[SI]
            CMP BL, 64
            JA UNO
            
            UNO:
            CMP BL, 91
            JB MAYUS
            JA MINUS
            
            MAYUS:
            MOV AH, 02
            MOV DL, BL
            INT 21H
            JMP FIN
            
            MINUS:
            MOV AH, 02
            MOV DL, BL
            SUB DL, 32
            INT 21H
            
            FIN:
            INC SI    
            
        LOOP CICLO
        
        
		

    RET
    MAIN ENDP
SEGCODE ENDS
END MAIN
