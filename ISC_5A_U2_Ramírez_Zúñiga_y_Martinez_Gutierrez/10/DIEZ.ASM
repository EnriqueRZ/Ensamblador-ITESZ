;LISTOOOOOOOOOOO
;
SEGPILA SEGMENT PARA STACK 'STACK'
        DB 512 DUP(0)
SEGPILA ENDS

SEGDATA SEGMENT PARA PUBLIC 'DATA'
		SALTO DB " ",10,13,'$'
		VEC1 DB 1,4,5,5,1,9,8,4,2,5,'$'
		VEC2 DB 1,2,3,4,3,6,7,8,9,1,'$'
		SALVA DW 0, '$'
		UNO DB ?
		DOS DB ?
SEGDATA ENDS

SEGCODE SEGMENT PARA PUBLIC 'CODE'
    MAIN PROC FAR
      ASSUME SS:SEGPILA,DS:SEGDATA,CS:SEGCODE

        PUSH DS
        MOV AX, 0
        PUSH AX
        MOV AX, seg SEGDATA
        MOV DS, AX
        
        MOV CX, 10
        MOV SI, 0
        CICLO1:
            PUSH CX
            
            MOV DL, VEC1[SI]
            
            MOV CX, 10
            MOV DI, 0
            CICLO2:
                MOV AL, VEC2[DI]
                CMP DL, AL
                JE IMPRIME
                JNE FIN
                
                IMPRIME:
                MOV AH, 02
                MOV DL, AL
                ADD DL, 48
                INT 21H
                
                FIN:
                INC DI 
                
            LOOP CICLO2
            
            INC SI
            POP CX

        LOOP CICLO1

    RET
    MAIN ENDP
SEGCODE ENDS
END MAIN
